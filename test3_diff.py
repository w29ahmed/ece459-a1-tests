test_output = open("output3.txt", "r")
expected_output = open("expected_output3.txt", "r")

lines = test_output.readlines()
expected_lines = expected_output.readlines()

for index, line in enumerate(expected_lines):
    # Skip first 2 lines
    if index < 2:
        continue

    # Split the string to just get the dependency set
    expected_dependencies_str = line.partition(" to install: ")[2]
    dependencies_str = lines[index].partition(" to install: ")[2]

    # Chop off the beginning and end quotations
    expected_dependencies_str = expected_dependencies_str[1:-2]
    dependencies_str = dependencies_str[1:-2]

    # Build dependency set
    expected_dependencies = set(expected_dependencies_str.split(", "))
    dependencies = set(dependencies_str.split(", "))

    if expected_dependencies != dependencies:
        print("Line {} does not match!".format(index+1))
