# ece459-a1-tests
Tests for assignment 1. Clone this repository into your working directory for assignment 1.

To run:
`bash testX.sh`

If you get no output, that means your output matches the expected output exactly. Any differing output will be spit out on a line-by-line basis.

# Tests
* `test1.sh`: Tests package parsing and async io. Specifically, the commands `ld`, `info`, `deps`, `deps-available`, `enq-verify`, and `execute` are used.
* `test2.sh`: Tests `transitive-dep-solution`. Parses the dependencies from the output into a set in order to account for duplicates and different ordering.
* `test3.sh`: Tests `how-to-install`. Parses the dependencies from the output into a set in order to account for duplicates and different ordering.
* `test4`: I couldn't find a way to automate this one, but while randomly picking packages for test3, I encountered some packages that were not installable due to a package not being available. This likely won't be tested, but might be worth checking that your program panics when running `how-to-install` on any of these packages:
    * `librust-libgit2-sys+libssh2-sys-dev`
    * `2048-qt`
    * `libhash-fieldhash-perl`
    * `tracker`
    * `php-illuminate-notifications`
    * `librust-diesel+deprecated-time-dev`
    * `dgedit`
    * `arriero`
